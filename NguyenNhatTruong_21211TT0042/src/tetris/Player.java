/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class Player implements Serializable{
    private static ArrayList<Player> _listPlayers;
    private String _name;
    private int _score;

    public static ArrayList<Player> getListPlayers() {
        return _listPlayers;
    }

    public String getName() {
        return _name;
    }

    public void setName(String _name) {
        this._name = _name;
    }

    public int getScore() {
        return _score;
    }

    public void setScore(int _score) {
        this._score = _score;
    }
    
    public Player(String name, int score) throws Exception {
        if (name.isEmpty()) {
            throw  new Exception("Ho ten khong duoc de trong");
        }
        
        _name = name;
        _score = score;
        
        if (_listPlayers == null) {
            _listPlayers = new ArrayList<>();
        }
        
        _listPlayers.add(this);
        
        sortListByScore();
        
    }
    
    public static void saveToFile() {
        try {
            FileOutputStream fos = new FileOutputStream("src/listplayers.txt");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            
            oos.writeObject(_listPlayers);
            
            fos.close();
            oos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
      public static void readData() {
        try {
            FileInputStream fis = new FileInputStream("src/listplayers.txt");
            ObjectInputStream ois = new ObjectInputStream(fis);
            
            _listPlayers = (ArrayList<Player>) ois.readObject();
            
            if (_listPlayers != null) {
                sortListByScore();
            }
            
            fis.close();
            ois.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Player other = (Player) obj;
        if (this._score != other._score) {
            return false;
        }
        if (!Objects.equals(this._name, other._name)) {
            return false;
        }
        return true;
    }
      
    public static void sortListByScore() {
        Collections.sort(_listPlayers, new Comparator<Player>() {
            @Override
            public int compare(Player o1, Player o2) {
                if (o1.getScore() > o2.getScore()) {
                    return -1;
                }
                else if (o1.getScore() == o2.getScore()) {
                    return 0;
                }
                return 1;
            }
        });
    }
}