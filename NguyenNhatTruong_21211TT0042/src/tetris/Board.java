/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris;

/**
 *
 * @author Administrator
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.sun.java.accessibility.util.EventID;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.List;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import org.w3c.dom.events.MouseEvent;

public class Board extends JPanel implements KeyListener, MouseListener, MouseMotionListener {

    public static final int ROWS = 20;
    public static final int COLUMNS = 10;
    public static final int BLOCK_SIZE = 30;
    private final BufferedImage _gameOverMessage = ImageLoader.loadImage("gameover.png");
    private final BufferedImage _play = ImageLoader.loadImage("play.png");
    private final BufferedImage _pause = ImageLoader.loadImage("pause.png");
    private final BufferedImage _resume = ImageLoader.loadImage("resume.png");
    public final int[][] _grid = new int[ROWS][COLUMNS];
    private final Rectangle _playBtn = new Rectangle(320, 147, 145, 35);
    private final Rectangle _pauseBtn = new Rectangle(320, 185, 145, 40);
    private final int normal = 600;
    private final int fast = 50;
    public Timer _loopper;
    private int _deplayTimeForMovement = normal;
    private long _beginTime;
    private Shape _currentShape;
    private int _score = 0;
    private int _mouseX;
    private int _mouseY;
    private boolean _leftClick = false;
    private boolean _gamePause = false;
    private boolean _gameOver = false;
    private boolean _gameRestart = false;

    public int[][] getGrid() {
        return _grid;
    }

    public Shape getCurrentShape() {
        return _currentShape;
    }

    public void setCurrentShape(Shape _currentShape) {
        this._currentShape = _currentShape;
    }

    public boolean isGameOver() {
        return _gameOver;
    }

    public void setGameOver(boolean _gameOver) {
        this._gameOver = _gameOver;
    }

    public boolean isGamePause() {
        return _gamePause;
    }

    public void setGamePause(boolean _gamePause) {
        this._gamePause = _gamePause;
    }

    public Board() {
        genWhiteBoard();
        _loopper = new Timer(1000 / 60, new shapeAction());
        _loopper.start();
        Shape shape = new Shape(this);
        this._currentShape = shape;
    }

    class shapeAction implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (System.currentTimeMillis() - _beginTime > _deplayTimeForMovement) {
                if (!_gamePause) {
                    _currentShape.moveDown();
                }
                if (_gameOver) {
                    _loopper.stop();
                    saveScore();
                }
                _beginTime = System.currentTimeMillis();
            }
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2D = (Graphics2D) g;
        startGame(g2D);

        if (this._gameRestart && !this._gamePause) {
            this._gameOver = false;
            genWhiteBoard();
            Shape shape = new Shape(this);
            this._currentShape = shape;
            startGame(g2D);
            this._score = 0;
            this._gameRestart = false;
        }

        if (this._gameOver) {
            drawGameOverScreen(g2D);
        }

        if (this._gamePause) {
            drawPauseScreen(g2D);
            this._gameRestart = false;
        }
    }

    public void startGame(Graphics2D g2D) {
        if (!_gameOver) {
            drawBoard(g2D);
            this._currentShape.makeShape(g2D);
            drawBorder(g2D);
        }

        drawRigthBoard(g2D);
    }

    public void genWhiteBoard() {
        for (int row = 0; row < ROWS; row++) {
            for (int col = 0; col < COLUMNS; col++) {
                this._grid[row][col] = 7;
            }
        }
    }

    public void drawCell(Graphics2D g2D, int col, int row, Color color) {
        Rectangle cell = new Rectangle(col * BLOCK_SIZE, row * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);
        g2D.draw(cell);
        g2D.setColor(color);
        g2D.fill(cell);
    }

    public void drawBoard(Graphics2D g2D) {
        for (int row = 0; row < ROWS; row++) {
            for (int col = 0; col < COLUMNS; col++) {
                drawCell(g2D, col, row, Shape.COLORS[this._grid[row][col]]);
            }
        }
    }

    public void drawBorder(Graphics2D g2D) {
        g2D.setColor(Color.black);
        for (int row = 0; row <= ROWS; row++) {
            g2D.drawLine(0, row * BLOCK_SIZE, COLUMNS * BLOCK_SIZE, row * BLOCK_SIZE);
        }

        for (int col = 0; col <= COLUMNS; col++) {
            g2D.drawLine(col * BLOCK_SIZE, 0, col * BLOCK_SIZE, ROWS * BLOCK_SIZE);
        }
    }

    public void drawRigthBoard(Graphics2D g2D) {
        Rectangle rightBoard = new Rectangle(300, 0, 200, 211);

        g2D.setColor(Color.DARK_GRAY);
        g2D.fill(rightBoard);
        g2D.setColor(Color.WHITE);

        drawScoreScreen(g2D);
        drawBtns(g2D);
        drawListPlayer(g2D);
    }

    public void drawScoreScreen(Graphics2D g2D) {
        g2D.fillRect(301, 1, 200, 105);
        g2D.setFont(new Font("tohama", Font.PLAIN, 25));
        g2D.setColor(Color.red);
        g2D.drawString("Score", 358, 48);
        g2D.drawString(String.format("%05d", _score), 356, 78);
    }

    public void drawListPlayer(Graphics2D g2D) {
        g2D.setColor(Color.decode("#cddc39"));
        g2D.fillRect(300, 211, 200, 640);
        g2D.setColor(Color.decode("#3f51b5"));

        g2D.setFont(new Font("Tohama", Font.BOLD, 18));
        g2D.drawString("TOP 5 PLAYERS", 320, 270);
        int y = 280;
        g2D.setFont(new Font("Arial", Font.PLAIN, 14));

        if (Player.getListPlayers() == null) {
            return;
        }
        for (int i = 0; i < Player.getListPlayers().size(); i++) {
            String info = String.format("%d. %s: %d", (i + 1), Player.getListPlayers().get(i).getName(), Player.getListPlayers().get(i).getScore());
            g2D.drawString(info, 319, y += 25);
            if (i == 4) {
                break;
            }
        }
    }

    public void drawBtns(Graphics2D g2D) {
        if (_playBtn.contains(_mouseX, _mouseY) && _leftClick) {
            g2D.drawImage(_play, 317, 117, 152, 35, this);
        } else {
            g2D.drawImage(_play, 320, 120, 145, 30, this);
        }

        if (_pauseBtn.contains(_mouseX, _mouseY) && _leftClick) {
            if (!_gamePause) {
                g2D.drawImage(_pause, 317, 160, 152, 35, this);
            } else {
                g2D.drawImage(_resume, 317, 160, 152, 35, this);
            }
        } else {
            if (!_gamePause) {
                g2D.drawImage(_pause, 320, 163, 145, 30, this);
            } else {
                g2D.drawImage(_resume, 320, 163, 145, 30, this);
            }
        }
        repaint();
    }

    public void drawGameOverScreen(Graphics2D g2D) {
        drawBoard(g2D);
        g2D.drawImage(_gameOverMessage, 45, 210, 200, 100, this);
    }

    public void drawPauseScreen(Graphics2D g2D) {
        g2D.setColor(Color.WHITE);
        g2D.fillRect(1, 8 * BLOCK_SIZE + 1, COLUMNS * BLOCK_SIZE - 1, 2 * BLOCK_SIZE - 1);
        g2D.setColor(Color.BLACK);
        g2D.setFont(new Font("Tohama", Font.BOLD, 30));
        g2D.drawString("Pause", 105, 280);
    }

    public void checkLine() {
        int size = _grid.length - 1;
        for (int i = _grid.length - 1; i > 0; i--) {
            int count = 0;
            for (int j = 0; j < _grid[0].length; j++) {
                if (_grid[i][j] != 7) {
                    count++;
                }
                _grid[size][j] = _grid[i][j];
            }
            if (count < _grid[0].length) {
                size--;
            } else {
                _score += 10;
            }
        }
    }

    public void saveScore() {
        String name = "";
        int choice = JOptionPane.showConfirmDialog(null, "Điểm của bạn là: " + this._score + ", bạn có muốn lưu không?");
        if (choice == 1 || choice == 2 || choice == -1) {
            return;
        }

        boolean success = true;
        do {
            try {
                name = JOptionPane.showInputDialog(null, "Nhập tên của bạn: ");
                if (name == null) {
                    break;
                }
                new Player(name, _score);
                Player.saveToFile();
                success = true;
            } catch (Exception e) {
                success = false;
                JOptionPane.showMessageDialog(null, "Tên không được để trống, vui lòng nhập lại !");
            }
        } while (!success);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP:
                this._currentShape.rotate();
                break;
            case KeyEvent.VK_DOWN:
                _deplayTimeForMovement = fast;
                break;

            case KeyEvent.VK_LEFT:
                this._currentShape.moveLeft();
                break;
            case KeyEvent.VK_RIGHT:
                this._currentShape.moveRight();
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_DOWN:
                _deplayTimeForMovement = normal;
                break;
        }
    }

    @Override
    public void mouseEntered(java.awt.event.MouseEvent e
    ) {
    }

    @Override
    public void mouseExited(java.awt.event.MouseEvent e
    ) {
    }

    @Override
    public void mouseClicked(java.awt.event.MouseEvent e
    ) {

    }

    @Override
    public void mouseReleased(java.awt.event.MouseEvent e
    ) {
        if (e.getButton() == java.awt.event.MouseEvent.BUTTON1) {
            this._leftClick = false;
        }
    }

    @Override
    public void mousePressed(java.awt.event.MouseEvent e
    ) {
        if (_pauseBtn.contains(_mouseX, _mouseY) && !_gameOver) {
            _gamePause = !_gamePause;
        }

        if (_playBtn.contains(_mouseX, _mouseY)) {
            _gameRestart = true;
            _gameOver = false;
            _loopper.start();
            _deplayTimeForMovement = normal;
        }

        if (e.getButton() == java.awt.event.MouseEvent.BUTTON1) {
            _leftClick = true;
        }
    }

    @Override
    public void mouseDragged(java.awt.event.MouseEvent e
    ) {
        this._mouseX = e.getX();
        this._mouseY = e.getY();
    }

    @Override
    public void mouseMoved(java.awt.event.MouseEvent e
    ) {
        this._mouseX = e.getX();
        this._mouseY = e.getY();
    }
}
