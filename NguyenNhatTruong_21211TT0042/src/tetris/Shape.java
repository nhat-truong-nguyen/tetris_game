/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tetris;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author Administrator
 */
public class Shape {

    public static final Color[] COLORS
            = {Color.red, Color.yellow, Color.green, Color.magenta, Color.blue, Color.cyan, Color.orange, Color.white};
    private Board _board;
    private int _id;
    private int _oriend;
    private int[][][] _layout;
    private int _x;
    private int _y;

    public Board getBoard() {
        return _board;
    }

    public void setBoard(Board _board) {
        this._board = _board;
    }

    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }

    public int getOriend() {
        return _oriend;
    }

    public void setOriend(int _oriend) {
        this._oriend = _oriend;
    }

    public int[][][] getLayout() {
        return _layout;
    }

    public void setLayout(int[][][] _layout) {
        this._layout = _layout;
    }

    public int getX() {
        return _x;
    }

    public void setX(int _x) {
        this._x = _x;
    }

    public int getY() {
        return _y;
    }

    public void setY(int _y) {
        this._y = _y;
    }

    public Shape(Board board) {
        _id = (int) (Math.random() * 7) % 7;
        _layout = Layout.TYPE[(int) (Math.random() * 4) % 4];
        _oriend = (int) (Math.random() * 4) % 4;
        _x = 3;
        _y = 0;
        _board = board;
    }

    public void makeShape(Graphics2D g2D) {
        for (int row = 0; row < _layout[_oriend].length; row++) {
            for (int col = 0; col < _layout[_oriend][0].length; col++) {
                if (_layout[_oriend][row][col] != 0) {
                    _board.drawCell(g2D, _x + col, _y + row, COLORS[_id]);
                }
            }
        }
    }

    public boolean isCollision(int nextRow, int nextCol, int[][] layout) {
        for (int row = 0; row < layout.length; row++) {
            for (int col = 0; col < layout[0].length; col++) {
                if (layout[row][col] != 0) {
                    if (nextCol + col < 0
                            || nextCol + col >= Board.COLUMNS
                            || _board._grid[nextRow + row][nextCol + col] != 7) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean isPlaned(int nextRow, int nextCol, int[][] layout) {
        for (int row = 0; row < layout.length; row++) {
            for (int col = 0; col < layout[0].length; col++) {
                if (layout[row][col] != 0) {
                    if (nextRow + row >= Board.ROWS
                            || _board._grid[nextRow + row][nextCol + col] != 7) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public void planedHandle() {
        for (int row = 0; row < _layout[_oriend].length; row++) {
            for (int col = 0; col < _layout[_oriend][0].length; col++) {
                if (_layout[_oriend][row][col] != 0) {
                    _board._grid[_y + row][_x + col] = _id;
                }
            }
        }
        
        if (_y == 0 && isPlaned(_y + 1, _x, _layout[_oriend])) {
            _board.setGameOver(true);
        }
    }

    public void rotate() {
        boolean check = isCollision(_y, _x, _layout[(_oriend + 1) % 4]);
        if (!check) {
            _oriend = (_oriend + 1) % 4;
        }
        _board.repaint();
    }

    public void moveLeft() {
        boolean check = isCollision(_y, _x - 1, _layout[_oriend]);
        if (!check) {
            _x--;
        }
        _board.repaint();
    }

    public void moveRight() {
        boolean check = isCollision(_y, _x + 1, _layout[_oriend]);
        if (!check) {
            _x++;
        }
        _board.repaint();
    }

    public void moveDown() {
        boolean check = isPlaned(_y + 1, _x, _layout[_oriend]);
        if (!check) {
            _y++;
        }
        else {
            planedHandle();
            _board.checkLine();
            _board.setCurrentShape(new Shape((_board)));
        }
        _board.repaint();
    }
}
